
# Tournament #

An app to handle simple (sport) tournaments using [Polymer](https://www.polymer-project.org).

* local storage is used as "backend"
* simple print styling to prepare tournaments
* simple responsivness for use on mobile devices
* schedule generation for teams included


## Usage ##

* copy `dist/index.html` which should include everything

### Simple Server ###

any static server should do the trick, here a python example

* `python3 -m http.server` or `python2 -m SimpleHTTPServer`

### Standalone ###

* open in `index.html` browser (might not work 100%)


## Contribute ##

1. install [npm](https://www.npmjs.com/) & [bower](http://bower.io/)
2. clone repository
3. install depedencies `package.json` & `bower.json` (includes [gulp](http://gulpjs.com/))
4. run `gulp serve` (simple http server for `app/` directory)
5. run `gulp watch` (sass to css)
6. navigate to `localhost:8000`

To build `dist/index.html` install [vulcanize](https://github.com/Polymer/vulcanize)
and run: `vulcanize --inline-scripts --inline-css app/index.html > dist/index.html`
