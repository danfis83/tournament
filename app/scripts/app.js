/*
Copyright (c) 2015 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

(function(document) {
  'use strict';

  // Grab a reference to our auto-binding template
  // and give it some initial binding values
  // Learn more about auto-binding templates at http://goo.gl/Dx1u2g
  var app = document.querySelector('#app');

  // init local storage
  app.store = Lockr;

  app.generateId = function () {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  };

  app.slugify = function (str) {

    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;",
        to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
      str = str.replace(from[i], to[i]);
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
      .replace(/\s+/g, '-') // collapse whitespace and replace by -
      .replace(/-+/g, '-'); // collapse dashes

    // Trim leading and trailing whitespace and dashes.
    str = str.replace(/^[\s|-]+|[\s|-]+$/g, '');

    return str;
  }

  app.Generator = function() {

    this.delimiter = '§';

    this.generate = function(object_list) {

      var i, j, game, l, new_idx;
      var games = [];
      var obj_list = [];
      var delimiter = this.delimiter;
      var game_rounds = 0;
      var _teams = [];
      var games_per_round = parseInt(object_list.length / 2, 10);
      for (i = 0; i < object_list.length; i++) { obj_list.push(i); }

      if (obj_list.length % 2 === 0) {
        // even
        //
        // 1 2  ->  1 3  ->  1 4
        // 3 4      4 2      2 3
        //
        // 1 2  ->  1 3  ->  1 5  ->  1 6  ->  1 4
        // 3 4      5 2      6 3      4 5      2 6
        // 5 6      6 4      4 2      2 3      3 5
        //
        game_rounds = obj_list.length - 1;
        _teams = obj_list.slice(0);
        for (i = 0; i < game_rounds; i++) {
          l = _teams.slice(0);
          for (j = 0; j < games_per_round; j++) {
            if (i % 2 === 1 && j === 0) {
              game = [l[1], l[0]];
            } else {
              game = [l[0], l[1]];
            }
            games.push(game);
            l = l.slice(2);
          }

          // spin the teams (clockwise)
          l = _teams.slice(0);
          for (j = 0; j < l.length; j++) {
            if (j === 0) {
              new_idx = j;
            } else if (j === 1) {
              new_idx = 3;
            } else if (j === 2) {
              new_idx = 1;
            } else if (j % 2 === 0) {
              new_idx = j - 2;
            } else if (j % 2 === 1) {
              new_idx = (j === l.length - 1) ? j - 1 : j + 2;
            }
            _teams[new_idx] = l[j];
          }
        }
      } else {
        // odd
        //
        // 1 2  ->  3 1  ->  5 3  ->  4 5  ->  2 4
        // 3 4      5 2      4 1      2 3      1 5
        // 5        4        2        1        3
        //
        game_rounds = obj_list.length;
        _teams = obj_list.slice(0);
        var _games = [];

        for (i = 0; i < game_rounds; i++) {
          l = _teams.slice(0);
          for (j = 0; j < games_per_round; j++) {
            game = l[0] + delimiter + l[1];
            _games.push(game);
            l = l.slice(2);
          }

          // spin the teams (clockwise)
          l = _teams.slice(0);
          for (j = 0; j < l.length; j++) {
            if (j % 2 === 0) {
              new_idx = (j === 0) ? 1 : j - 2;
            } else if (j % 2 === 1) {
              new_idx = (j === l.length - 2) ? j + 1 : j + 2;
            }
            _teams[new_idx] = l[j];
          }

        }

        // sorting/ordering
        var game_count = _games.length;
        var indexes = [];
        var _tmp = [];
        var k, game_split, index, _game, _team_idx, game_idx;

        var _sort = function(a, b) {
          return a.index - b.index;
        };

        for (i = 0; i < game_count; i++) {
          indexes = [];
          _tmp = [];
          for (j = 0; j < games.length; j++) {
            game_split = games[j].split(delimiter);
            for (k = 0; k < game_split.length; k++) {
              _tmp.push(game_split[k]);
            }
          }
          _tmp.reverse();

          for (j = 0; j < _games.length; j++) {
            _game = _games[j];
            index = -1;
            game_split = _game.split(delimiter);
            for (k = 0; k < game_split.length; k++) {
              l = _tmp.indexOf(game_split[k]);
              if (l > -1) {
                _team_idx = l;
                if (_team_idx % 2 === 1) {
                  _team_idx--;
                }
                index -= _team_idx;
              } else {
                index -= _teams.length;
              }
            }

            indexes.push({game: j, index: index});
          }

          indexes.sort(_sort);

          game_idx = indexes[0].game;
          games.push(_games[game_idx]);
          _games.splice(game_idx, 1);
        }

        _games = games;
        games = [];
        _games.forEach(function(_game) {
          games.push(_game.split(delimiter));
        });

      }

      // reassign indexes to object_list
      var result = [];
      games.forEach(function(game) {
        result.push([object_list[game[0]], object_list[game[1]]]);
      });

      return result;
    };

  };

  // Listen for template bound event to know when bindings
  // have resolved and content has been stamped to the page
  app.addEventListener('dom-change', function() {
    // console.log('Our app is ready to rock!');
  });

  // See https://github.com/Polymer/polymer/issues/1381
  window.addEventListener('WebComponentsReady', function() {
    // imports are loaded and elements have been registered

    // scroll handler
    var header = document.querySelector('header');
    var main = document.querySelector('main');
    var offset = main.offsetTop - header.offsetHeight;
    window.addEventListener('scroll', function(e) {
      if (window.scrollY >= offset)
        document.querySelector('body').classList.add('scrolled')
      else
        document.querySelector('body').classList.remove('scrolled');
    });

    // set header position
    var resize = function (e) {
      var offset = document.querySelector('main .container').offsetLeft + 'px';
      var h2 = header.querySelector('h2');
      var h3 = header.querySelector('h3');
      h2.style.setProperty('padding-left', offset);
      h3.style.setProperty('padding-left', offset);
      h2.classList.add('show');
      h3.classList.add('show');
    };

    window.addEventListener('resize', resize);
    resize();

  });

})(document);
