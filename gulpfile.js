
'use strict';

var gulp = require('gulp'),
    gls = require('gulp-live-server'),
    sass = require('gulp-sass'),
    vulcanize = require('gulp-vulcanize');


// server

gulp.task('serve', function () {
  var server = gls.static('app', 8000);
  server.start();

  gulp.watch(['app/**/*.html', 'app/**/*.js', 'app/**/*.css'], function (file) {
    server.notify.apply(server, [file]);
  });
});


// sass

gulp.task('sass', function () {
  gulp.src('app/sass/*.sass')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('app/styles/'));
});


// watch

gulp.task('sass:watch', function () {
  gulp.watch('app/sass/*.sass', ['sass']);
});

gulp.task('watch', ['sass:watch']);


// build

gulp.task('build:vulcanize', function () {
  gulp.src('app/index.html')
    .pipe(vulcanize({
      inlineScripts: true,
      inlineCss: true,
      stripComments: true
    }))
    .pipe(gulp.dest('dist/'));
});

gulp.task('build:cordova', function () {
  // TODO: cd into cordova dir and run 'cordova build'
  gulp.src('dist/index.html')
    .pipe(gulp.dest('cordova/www'));

  gulp.src('app/images/icon.png')
    .pipe(gulp.dest('cordova/www/img'));

  gulp.src('app/images/splash.png')
    .pipe(gulp.dest('cordova/www/img'));

  gulp.src('app/images/splash_land.png')
    .pipe(gulp.dest('cordova/www/img'));

  gulp.src('app/images/splash_port.png')
    .pipe(gulp.dest('cordova/www/img'));
});
